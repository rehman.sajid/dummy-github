import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchBoxComponent } from './search-box/search-box.component';

const routes: Routes = [
  { path: '', component: SearchBoxComponent , data: { animation: 'isLeft'} },
  { path: 'details/:username', loadChildren: () => import('./user-details/user-details.module').then(m => m.UserDetailsModule), data: {animation: 'isRight'} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
