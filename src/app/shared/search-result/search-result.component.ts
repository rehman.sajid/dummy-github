import { Component, Input } from '@angular/core';
import { GitUser } from '../../gituser';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent {
  @Input() gitUser?: GitUser
}
