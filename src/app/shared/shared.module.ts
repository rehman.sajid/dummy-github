import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchResultComponent } from './search-result/search-result.component';
import { RouterLink } from '@angular/router';

@NgModule({
  declarations: [
    SearchResultComponent
  ],
  imports: [
    CommonModule,
    RouterLink
  ],
  exports:[ SearchResultComponent ]
})
export class SharedModule { }
