import { Component, Input, OnInit } from '@angular/core';
import { repository } from 'src/app/repository';

@Component({
  selector: 'app-repository-list',
  templateUrl: './repository-list.component.html',
  styleUrls: ['./repository-list.component.css']
})
export class RepositoryListComponent {
  @Input() repos?: repository[];
}