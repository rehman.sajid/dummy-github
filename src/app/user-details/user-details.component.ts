import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GitUser } from '../gituser';
import { repository } from '../repository';
import { GithubApiService } from '../github-api.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
})
export class UserDetailsComponent {

  constructor(private route: ActivatedRoute, private gitService: GithubApiService, private location: Location){}

  username: String | null | undefined
  user?: GitUser;

  followers?: GitUser[] = [];
  repos?: repository[] = [];
  showRepos: Boolean = true;

  ngOnInit(){
    this.route.paramMap.subscribe(params => {
      this.username = params.get('username');
      this.getUserDetails();
      this.getUserRepos();
      this.getFollowers();
      this.moveWindowToTop();
    })
  }

  moveWindowToTop(){
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  getUserRepos(){
    this.gitService.getRepos(this.username?? "").subscribe(
      repo => this.repos = repo
    )
  }

  getFollowers(){
    this.gitService.getFollowers(this.username?? "").subscribe(
      followers => this.followers = followers
    )
  }

  getUserDetails(){
    this.gitService.get(this.username??"").subscribe(
      user => this.user = user
    )
  }

  goBack(): void{
    this.location.back();
  }

  toggleComponent(){
    this.showRepos = !this.showRepos
  }
}
