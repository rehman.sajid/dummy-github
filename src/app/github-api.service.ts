import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GitUser, GitUsers } from './gituser';
import { repository } from './repository';

@Injectable({
  providedIn: 'root'
})

export class GithubApiService {

  constructor(private http: HttpClient) {}

  private github_base_api_url = 'https://api.github.com'

  searchUser(searchTerm: String): Observable<GitUsers>{
    return this.http.get<GitUsers>(this.github_base_api_url + `/search/users?q=${searchTerm}`)
  }

  getUsers(): Observable<GitUser []>{
    return this.http.get<GitUser []>(this.github_base_api_url + '/users')
  }

  get(user: String): Observable<GitUser>{
    return this.http.get<GitUser>(this.github_base_api_url + `/users/${user}`)
  }

  getRepos(user: String): Observable<repository[]>{
    return this.http.get<repository []>(this.github_base_api_url + `/users/${user}/repos`)
  }

  getFollowers(user: String): Observable<GitUser[]>{
    return this.http.get<GitUser []>(this.github_base_api_url + `/users/${user}/followers`)
  }
}