import { Component } from '@angular/core';
import { GithubApiService } from '../github-api.service';
import { GitUser } from '../gituser';
import { OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {

  form: FormGroup;

  constructor(private gitService: GithubApiService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      userInput: ['']
    });
  }

  isLoading = true
  users?: GitUser[] = []
  searchTerm = new Subject<String>();

  ngOnInit(){
    this.getUsers("");

    this.searchTerm.subscribe((v) => {
      this.isLoading = true;
    });

    this.searchTerm.pipe(
      debounceTime(900)
    ).subscribe((v) => {
      this.getUsers(v);
    });
  }

  getUsers(searchTerm: String): void{
    if (searchTerm){
      this.gitService.searchUser(searchTerm).subscribe(users => {
        this.users = users.items;
        this.isLoading = false;
      });
    }else{
      this.gitService.getUsers().subscribe(users => {
        this.users = users;
        this.isLoading = false;
      });
    }
  }

}