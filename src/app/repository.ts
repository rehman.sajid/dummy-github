export interface repository{
    name: String;
    html_url: String;
    description: String;
}