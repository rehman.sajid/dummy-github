export interface GitUser{
    id: Number;
    url: String;
    login: String,
    followers_url: String;
    following_url: String;
    avatar_url: String;
    html_url: String;
    repos_url: String;
    bio?: String;
    name?: String;
    followers?: String;
    following?: String;
}

export interface GitUsers{
    items: GitUser[]
}