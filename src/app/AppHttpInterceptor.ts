import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor,HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

  constructor(){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const gitAuthToken = environment.githubKey
    const modRequest = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${gitAuthToken}`)
    });
    return next.handle(modRequest)
  }
}