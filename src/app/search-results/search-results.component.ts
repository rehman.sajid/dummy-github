import { Component, Input } from '@angular/core';
import { GitUser } from '../gituser';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent {
  @Input() users?: GitUser[] = []
}